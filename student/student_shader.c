/*!
 * @file 
 * @brief This file contains implemenation of phong vertex and fragment shader.
 *
 * @author Tomáš Milet, imilet@fit.vutbr.cz
 */

#include<math.h>
#include<assert.h>

#include"student/student_shader.h"
#include"student/gpu.h"
#include"student/uniforms.h"

/// \addtogroup shader_side Úkoly v shaderech
/// @{

void phong_vertexShader(
    GPUVertexShaderOutput     *const output,
    GPUVertexShaderInput const*const input ,
    GPU                        const gpu   ){
  /// \todo Naimplementujte vertex shader, který transformuje vstupní vrcholy do clip-space.<br>
  /// <b>Vstupy:</b><br>
  /// Vstupní vrchol by měl v nultém atributu obsahovat pozici vrcholu ve world-space (vec3) a v prvním
  /// atributu obsahovat normálu vrcholu ve world-space (vec3).<br>
  /// <b>Výstupy:</b><br>
  /// Výstupní vrchol by měl v nultém atributu obsahovat pozici vrcholu (vec3) ve world-space a v prvním
  /// atributu obsahovat normálu vrcholu ve world-space (vec3).
  /// Výstupní vrchol obsahuje pozici a normálu vrcholu proto, že chceme počítat osvětlení ve world-space ve fragment shaderu.<br>
  /// <b>Uniformy:</b><br>
  /// Vertex shader by měl pro transformaci využít uniformní proměnné obsahující view a projekční matici.
  /// View matici čtěte z uniformní proměnné "viewMatrix" a projekční matici čtěte z uniformní proměnné "projectionMatrix".
  /// Zachovejte jména uniformních proměnných a pozice vstupních a výstupních atributů.
  /// Pokud tak neučiníte, akceptační testy selžou.<br>
  /// <br>
  /// Využijte vektorové a maticové funkce.
  /// Nepředávajte si data do shaderu pomocí globálních proměnných.
  /// Pro získání dat atributů použijte příslušné funkce vs_interpret* definované v souboru program.h.
  /// Pro získání dat uniformních proměnných použijte příslušné funkce shader_interpretUniform* definované v souboru program.h.
  /// Vrchol v clip-space by měl být zapsán do proměnné gl_Position ve výstupní struktuře.<br>
  /// <b>Seznam funkcí, které jistě použijete</b>:
  ///  - gpu_getUniformsHandle()
  ///  - getUniformLocation()
  ///  - shader_interpretUniformAsMat4()
  ///  - vs_interpretInputVertexAttributeAsVec3()
  ///  - vs_interpretOutputVertexAttributeAsVec3()
  (void)output;
  (void)input;
  (void)gpu;

}

void phong_fragmentShader(
    GPUFragmentShaderOutput     *const output,
    GPUFragmentShaderInput const*const input ,
    GPU                          const gpu   ){
  /// \todo Naimplementujte fragment shader, který počítá phongův osvětlovací model s phongovým stínováním.<br>
  /// <b>Vstup:</b><br>
  /// Vstupní fragment by měl v nultém fragment atributu obsahovat interpolovanou pozici ve world-space a v prvním
  /// fragment atributu obsahovat interpolovanou normálu ve world-space.<br>
  /// <b>Výstup:</b><br> 
  /// Barvu zapište do proměnné color ve výstupní struktuře.<br>
  /// <b>Uniformy:</b><br>
  /// Pozici kamery přečtěte z uniformní proměnné "cameraPosition" a pozici světla přečtěte z uniformní proměnné "lightPosition".
  /// Zachovejte jména uniformních proměnný.
  /// Pokud tak neučiníte, akceptační testy selžou.<br>
  /// <br>
  /// Dejte si pozor na velikost normálového vektoru, při lineární interpolaci v rasterizaci může dojít ke zkrácení.
  /// Zapište barvu do proměnné color ve výstupní struktuře.
  /// Shininess faktor nastavte na 40.f
  /// Difuzní barvu materiálu nastavte na čistou zelenou.
  /// Spekulární barvu materiálu nastavte na čistou bílou.
  /// Barvu světla nastavte na bílou.
  /// Nepoužívejte ambientní světlo.<br>
  /// <b>Seznam funkcí, které jistě využijete</b>:
  ///  - shader_interpretUniformAsVec3()
  ///  - fs_interpretInputAttributeAsVec3()
  (void)output;
  (void)input;
  (void)gpu;

// output structure
// /**
//  * @brief This struct represents output data of fragment shader.
//  */
// struct GPUFragmentShaderOutput{
//   Vec4  color;///< color of the fragment
//   float depth;///< depth of the fragment
// };
/*
// get coords (screenspace coords)
printf("input->coords.data[0]: %f\n", input->coords.data[0]);
printf("input->coords.data[1]: %f\n", input->coords.data[1]);

// get depth of the fragment
printf("input->depth: %f\n", input->depth);

// get positions of camera
Vec3 * camera;
camera = shader_interpretUniformAsVec3(gpu_getUniformsHandle(gpu), getUniformLocation(gpu,"cameraPosition"));
//normalize_Vec3 (camera, camera);
printf("camera X: %f\n", camera->data[0]);
printf("camera Y: %f\n", camera->data[1]);
printf("camera Z: %f\n", camera->data[2]);

// get positions of light
Vec3 * light;
light = shader_interpretUniformAsVec3(gpu_getUniformsHandle(gpu), getUniformLocation(gpu,"lightPosition"));
//normalize_Vec3 (light, light);
printf("light X: %f\n", light->data[0]);
printf("light Y: %f\n", light->data[1]);
printf("light Z: %f\n", light->data[2]);

// get positions of attribute0 - position
Vec3 * att0;
att0 = fs_interpretInputAttributeAsVec3(gpu, input->attributes.attributes, 0);
//normalize_Vec3 (att0, att0);
printf("att0 X: %f\n", att0->data[0]);
printf("att0 Y: %f\n", att0->data[1]);
printf("att0 Z: %f\n", att0->data[2]);

// get positions of attribute1 - normal vector
Vec3 * att1;
att1 = fs_interpretInputAttributeAsVec3(gpu, input->attributes.attributes, 1);
//normalize_Vec3 (att1, att1);
printf("att1 X: %f\n", att1->data[0]);
printf("att1 Y: %f\n", att1->data[1]);
printf("att1 Z: %f\n", att1->data[2]);

output->depth = input->depth;

// difuzion part
Vec3 I_D;
init_Vec3(&I_D, 0, 1, 0); // green

// specture color of material
Vec3 I_L;
init_Vec3(&I_L, 1, 1, 1); // white

// color of light
Vec3 r_s;
init_Vec3(&r_s, 1, 1, 1); // white

// vec. L = |att0 light| = light - att0
Vec3 L;
init_Vec3(&L, (light->data[0] - att0->data[0]), (light->data[1] - att0->data[1]), 
  (light->data[2] - att0->data[2]));

//normalize_Vec3 (&L, &L);

printf("L.data[0]: %f\n", L.data[0]);
printf("L.data[1]: %f\n", L.data[1]);
printf("L.data[2]: %f\n", L.data[2]);

// vec. V = |att0 camera| = camera - att0
Vec3 V;
init_Vec3(&V, (camera->data[0] - att0->data[0]), (camera->data[1] - att0->data[1]), 
  (camera->data[2] - att0->data[2]));

//normalize_Vec3 (&V, &V);

printf("V.data[0]: %f\n", V.data[0]);
printf("V.data[1]: %f\n", V.data[1]);
printf("V.data[2]: %f\n", V.data[2]);

// vec. R = 2 * (N * L) * N - L
// N == att1
Vec3 R;

R = multiply_Vec3_Vec3(att1, &L);
multiply_Vec3_Float(&R, &R, 2);
R = multiply_Vec3_Vec3(&R, att1);
sub_Vec3 (&R, &R, &L);

// R.data[0] = 2 * (att1->data[0] * L.data[0]) * att1->data[0] - L.data[0];
// R.data[1] = 2 * (att1->data[1] * L.data[1]) * att1->data[1] - L.data[1];
// R.data[2] = 2 * (att1->data[2] * L.data[2]) * att1->data[2] - L.data[2];

//normalize_Vec3 (&R, &R);

printf("R.data[0]: %f\n", R.data[0]);
printf("R.data[1]: %f\n", R.data[1]);
printf("R.data[2]: %f\n", R.data[2]);

// // scalar multiple
// float cosAlfa = (V.data[0]*L.data[0]+V.data[1]*L.data[1]+V.data[2]*L.data[2]) /
//   (sqrtf( powf((V.data[0]), 2) + powf((V.data[1]), 2) + powf((V.data[2]), 2)) *
//   sqrtf( powf((L.data[0]), 2) + powf((L.data[1]), 2) + powf((L.data[2]), 2)) );

// printf("cosAlfa: %f\n", cosAlfa);

// I_P = I_D + I_L * r_s * cosAlfa
// n_s = 40
// output->color.data[0] = I_D.data[0] + I_L.data[0] * r_s.data[0] *
//   cosAlfa;

// output->color.data[1] = I_D.data[1] + I_L.data[1] * r_s.data[1] *
//   cosAlfa;

// output->color.data[2] = I_D.data[2] + I_L.data[2] * r_s.data[2] *
//   cosAlfa;


// second alternative
// I_P = I_D + I_L * r_s * (V * R)^n_s
// n_s = 40
Vec3 I_P;
I_P = multiply_Vec3_Vec3(&V, &R);
powf( (I_P.data[0]), 40);
powf( (I_P.data[1]), 40);
powf( (I_P.data[2]), 40);

I_P = multiply_Vec3_Vec3(&I_P, &r_s);
I_P = multiply_Vec3_Vec3(&I_P, &I_L);

I_P.data[0] += I_D.data[0];
I_P.data[1] += I_D.data[1];
I_P.data[2] += I_D.data[2];

normalize_Vec3 (&I_P, &I_P);

output->color.data[0] = I_P.data[0];
output->color.data[1] = I_P.data[1];
output->color.data[2] = I_P.data[2];


// output->color.data[0] = I_D.data[0] + I_L.data[0] * r_s.data[0] *
//   powf( (V.data[0] * R.data[0]), 40);

// output->color.data[1] = I_D.data[1] + I_L.data[1] * r_s.data[1] *
//   powf( (V.data[1] * R.data[1]), 40);

// output->color.data[2] = I_D.data[2] + I_L.data[2] * r_s.data[2] *
//   powf( (V.data[2] * R.data[2]), 40);


// result 
printf("output->color.data[0]: %e\n", output->color.data[0]);
printf("output->color.data[1]: %e\n", output->color.data[1]);
printf("output->color.data[2]: %e\n", output->color.data[2]);

// this shoud be there
// output->color.data[0] = 1.7394564955e-10f;
// output->color.data[1] = 9.8787826300e-01f;
// output->color.data[2] = 1.7394564955e-10f;
// output->color.data[3] = 1.0000000000e+00f;
*/
}

/// @}

Vec3 multiply_Vec3_Vec3(Vec3 * u, Vec3 * v) {
  Vec3 result;
  result.data[0] = u->data[1]*v->data[2] - v->data[1]*u->data[2];
  result.data[1] = u->data[2]*v->data[0] - v->data[2]*u->data[0];
  result.data[2] = u->data[0]*v->data[1] - v->data[0]*u->data[1];

  return result;
}